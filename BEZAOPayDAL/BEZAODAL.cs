﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using BEZAOPayDAL.Models;

namespace BEZAOPayDAL
{
    public class BEZAODAL
    {
        private readonly string _connectionString;
        // private SqlConnection _sqlConnection = null;

        public BEZAODAL() :
           this(@"Data Source=(localdb)\ProjectsV13;Initial Catalog=BEZAOPay;Integrated Security=True")
        {
            
        }

        public BEZAODAL(string connectionString)
        {
            _connectionString = connectionString;
        }


        private SqlConnection _sqlConnection = null;
        private void OpenConnection()
        {
            _sqlConnection = new SqlConnection { ConnectionString = _connectionString };
            _sqlConnection.Open();
        }

        private void CloseConnection()
        {
            if (_sqlConnection?.State == ConnectionState.Open) // != ConnectionState.Close
            {
                _sqlConnection?.Close();
            }
        }


        public IEnumerable<User> GetAllUsers()
        {
            OpenConnection();

            var users = new List<User>();

            var query = @"SELECT * FROM USERS";

            using (var command = new SqlCommand(query, _sqlConnection))
            {
                command.CommandType = CommandType.Text;
                var reader = command.ExecuteReader(CommandBehavior.CloseConnection);

                while (reader.Read())
                {
                    users.Add(new User
                    {
                        //Id = (int) reader["Id"],
                        Name = (string)reader["Name"],
                        Email = (string)reader["Email"]


                    });
                }
                reader.Close();
                //CloseConnection();//omissision because of the using statement
            }

            return users;

        }

        public void CreateUser(User user)
        {
            OpenConnection();

            string sqlQuery = $"INSERT INTO USERS (Name, Email) VALUES ('{user.Name}', '{user.Email}')";

            using (var command = new SqlCommand(sqlQuery, _sqlConnection))
            {
                command.CommandType = CommandType.Text;
                int record = command.ExecuteNonQuery();
                Console.WriteLine($"{record} records added");
            }

            CloseConnection();
        }

        public void CreateUser(string name, string email)
        {
            OpenConnection();

            string sqlCommand = $"INSERT INTO USERS (Name, Email) VALUES ('{name}', '{email}')";

            using (var command = new SqlCommand(sqlCommand, _sqlConnection))
            {
                command.CommandType = CommandType.Text;
                command.Parameters.AddWithValue("Name", name);
                command.Parameters.AddWithValue("Email", email);
                int record = command.ExecuteNonQuery();
                Console.WriteLine($"{record} records added with Name {command.Parameters["Name"].Value}");
            }

            CloseConnection();
        }

        public void CreateAccount(Account account)
        {
            OpenConnection();
            string sqlQuery = $"INSERT INTO ACCOUNTS (UserId, Account_Number, Balance) " +
                $"VALUES ('{account.UserId}', '{account.AccountNumber}', '{account.Balance}')";
            using (var command = new SqlCommand(sqlQuery, _sqlConnection))
            {
                command.CommandType = CommandType.Text;
                command.Parameters.AddWithValue("UserId", account.UserId);
                command.Parameters.AddWithValue("Account_Number", account.AccountNumber);
                command.Parameters.AddWithValue("Balance", account.Balance);
                int record = command.ExecuteNonQuery();
                Console.WriteLine($"{record} records added with Name {command.Parameters["UserId"].Value}");
            }
            CloseConnection();
        }

        public void InsertUserProcedure(string name, string email)
        {
            OpenConnection();
            using (var command = new SqlCommand("InsertUser", _sqlConnection))
            {
                command.CommandType = CommandType.StoredProcedure;
                var parameter = new SqlParameter
                {
                    ParameterName = "@name",
                    SqlDbType = SqlDbType.NVarChar,
                    Value = name,
                    Direction = ParameterDirection.Input

                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@email",
                    SqlDbType = SqlDbType.NVarChar,
                    Value = email,
                    Direction = ParameterDirection.Input

                };
                command.Parameters.Add(parameter);

                int record = command.ExecuteNonQuery();
                Console.WriteLine($"{record} records added");

                foreach (var item in command.Parameters)
                {
                    Console.WriteLine($"items: {item}");
                }
            }
            CloseConnection();


        }

        public void InsertAccountProcedure(int UserId, int Account_Number, decimal Balance)
        {
            OpenConnection();
            using (var command = new SqlCommand("InsertAccount", _sqlConnection))
            {
                command.CommandType = CommandType.StoredProcedure;
                var parameter = new SqlParameter
                {
                    ParameterName = "@UserID",
                    SqlDbType = SqlDbType.NVarChar,
                    Value = UserId,
                    Direction = ParameterDirection.Input

                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@AccountNumber",
                    SqlDbType = SqlDbType.NVarChar,
                    Value = Account_Number,
                    Direction = ParameterDirection.Input

                };
                command.Parameters.Add(parameter);

                parameter = new SqlParameter
                {
                    ParameterName = "@Balance",
                    SqlDbType = SqlDbType.NVarChar,
                    Value = Balance,
                    Direction = ParameterDirection.Input

                };
                command.Parameters.Add(parameter);

                int record = command.ExecuteNonQuery();
                Console.WriteLine($"{record} records added");

                foreach (var item in command.Parameters)
                {
                    Console.WriteLine($"items: {item}");
                }
            }
            CloseConnection();


        }
        public void DeleteUser(int id)
        {
            OpenConnection();
            var sqlQuery = $"Delete from Users where Id = '{id}'"; 
            using (SqlCommand command = new SqlCommand(sqlQuery, _sqlConnection))
            {
                try
                {
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                    Console.WriteLine("delete successful");
                }
                catch (SqlException ex)
                {
                    Exception error = new Exception("sorry! Id cant be found", ex);
                    throw error;
                }
            }
            CloseConnection();
        }

        public void UpdateUser(Account account)
        {
            OpenConnection();
            string sqlQuery = $" UPDATE Accounts SET Balance = '{account.Balance + account.Balance}' WHERE Id = '{account.Id}'";
            using (SqlCommand command = new SqlCommand(sqlQuery, _sqlConnection))
            {
                command.ExecuteNonQuery(); 

            }
            CloseConnection();
        }
        public void SendMoney(Account account, Transaction transaction)
        {
            OpenConnection();
            string sqlQuery1 = $"Update Accounts Set Balance = '{account.Balance + account.Balance}' Where Id = '{account.Id}'";
            string sqlQuery2 = $"Insert Into Transactions (UserId, Mode, Amount, Time) Values" +
            $" ('{transaction.UserId}', '{transaction.Mode}', '{transaction.Amount}', '{transaction.Time}')";
            var commandSqlQuery1 = new SqlCommand(sqlQuery1, _sqlConnection);
            var commandSqlQuery2 = new SqlCommand(sqlQuery2, _sqlConnection);
            SqlTransaction tx = null;
            try 
            { 
                tx = _sqlConnection.BeginTransaction();
                commandSqlQuery1.Transaction = tx;
                commandSqlQuery2.Transaction = tx;

                commandSqlQuery1.ExecuteNonQuery();
                commandSqlQuery2.ExecuteNonQuery();
                tx.Commit();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                tx?.Rollback();
            }
            finally
            {
                CloseConnection();
            }


        }
    }
    



        
   
}
