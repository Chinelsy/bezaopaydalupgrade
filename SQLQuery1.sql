﻿CREATE PROCEDURE InsertAccount
@UserID int,
@AccountNumber int,
@Balance decimal(38, 2)
AS
INSERT INTO Accounts(UserId, Account_Number, Balance) VALUES (@UserID, @AccountNumber, @Balance)

